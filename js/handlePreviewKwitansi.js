

$(document).ready(function () {

    var tanggal = Math.floor(Math.random() * 10000) + 1;
    //alert(tanggal);
    $("#inputNokw").val("CU#" + tanggal);
    $('#preview_numberKw').html("<b>No. CU#" + tanggal + "</b>")

    $('#inputDate').datepicker({
        format: 'yyyy-mm-dd'
    });

    var kuitansiNumber = "CU#" + tanggal;
    loadBarcode(kuitansiNumber);
    function loadBarcode(kuitansiNumber){
        JsBarcode("#preview_barcode", kuitansiNumber, {
            height: 30
        });
    }
    $('#formCreate').submit(function(event){
        event.preventDefault();
        loadBarcode(kuitansiNumber)
        
        // $.ajax({})
        getPrintContent();
    })

    $('#inputTerimaDari').change(function () {
        var terimaDari = $(this).val();
        $('#preview_terimaDari').html(terimaDari);
    })
    
    $('#inputDate').change(function(){
        var tglskr = $(this).val();
        var bulan = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        var t = tglskr.substr(8, 2);
        var b = tglskr.substr(5, 2);
        var th = tglskr.substr(0, 4);
        $('#preview_place').html("Depok, " + t + " " + bulan[parseInt(b)] + " " + th);
    })
    $('#inputKet').keyup(function(){
        var keterangan = $('#inputKet').val();
        $('#preview_keterangan').html(keterangan);
    })

    var rupiah = document.getElementById('rupiah');
    
    $('#inputNominal').keyup(function () {
        $(this).val(formatRupiah(this.value, 'Rp. '));
        var text = $('#preview_terbilang').text();
        if (text.length > 77) {
            $('#preview_bgText').html("<img src='img/terbilang_ex.png'>");
            $('#preview_terbilang_container').css('top', '-70px')
        } else {
            $('#preview_bgText').html("<img src='img/terbilang.png'>");
            $('#preview_terbilang_container').css('top', '-50px')
        }

        var bilangan = $(this).val().replace(/[^\w\s]/gi, '').split(' ')[1];

        var number_string = bilangan,
            sisa = number_string.length > 2 ? number_string.length % 3 : 0,
            rupiah = number_string.substr(0, sisa),
            ribuan = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        var kalimat = "";
        var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
        var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
        var tingkat = new Array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');
        var panjang_bilangan = bilangan.length;

        /* pengujian panjang bilangan */
        if (panjang_bilangan > 15) {
            kalimat = "Diluar Batas";
        } else {
            /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
            for (i = 1; i <= panjang_bilangan; i++) {
                angka[i] = bilangan.substr(-(i), 1);
            }

            var i = 1;
            var j = 0;

            /* mulai proses iterasi terhadap array angka */
            while (i <= panjang_bilangan) {
                subkalimat = "";
                kata1 = "";
                kata2 = "";
                kata3 = "";

                /* untuk Ratusan */
                if (angka[i + 2] != "0") {
                    if (angka[i + 2] == "1") {
                        kata1 = "Seratus";
                    } else {
                        kata1 = kata[angka[i + 2]] + " Ratus";
                    }
                }

                /* untuk Puluhan atau Belasan */
                if (angka[i + 1] != "0") {
                    if (angka[i + 1] == "1") {
                        if (angka[i] == "0") {
                            kata2 = "Sepuluh";
                        } else if (angka[i] == "1") {
                            kata2 = "Sebelas";
                        } else {
                            kata2 = kata[angka[i]] + " Belas";
                        }
                    } else {
                        kata2 = kata[angka[i + 1]] + " Puluh";
                    }
                }

                /* untuk Satuan */
                if (angka[i] != "0") {
                    if (angka[i + 1] != "1") {
                        kata3 = kata[angka[i]];
                    }
                }

                /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
                if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
                    subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
                }

                /* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
                kalimat = subkalimat + kalimat;
                i = i + 3;
                j = j + 1;
            }

            /* mengganti Satu Ribu jadi Seribu jika diperlukan */
            if ((angka[5] == "0") && (angka[6] == "0")) {
                kalimat = kalimat.replace("Satu Ribu", "Seribu");
            }
        }
       
        $('#preview_terbilang').html(kalimat + " Rupiah")
        $('#preview_tertulis').html(rupiah + ",-")
       
    })

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    $('#inputTerimaDari').focus();
});
